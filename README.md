Tasks to answer in your own README.md that you submit on Canvas:

1.  See logger.log, why is it different from the log to console?

	The log is easier to filter.  There can be multiple logs which contain only certain aspects of the application.


1.  Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']

	This appears in the logger.log file. Caused by line 17 in Tester.java


1.  What does Assertions.assertThrows do?

	It asserts that the block of code will throw the specified type of exception, and returns that type of exception.


1.  See TimerException and there are 3 questions
    1.  What is serialVersionUID and why do we need it? (please read on Internet)

		It is unique to this particular version of the class.  It is a way of verifying that a saved object is compatible.
		

    1.  Why do we need to override constructors?

		Constructors must be overriden because they are not inherited from the super class.


    1.  Why we did not override other Exception methods?	

		Letting them remain the same as the super class was sufficient for our purposes.


1.  The Timer.java has a static block static {}, what does it do? (determine when called by debugger)

	The code inside this block gets run when the class is instantiated.


1.  What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)

	md is an abreviation for markdown, which is a system to easily format text for display by a system that knows how to read it.  Bitbucket tries to use it in README files and in pull requests.


1.  Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)

	A null pointer is used in subtraction, which causes a null pointer exception.  Give timeNow some value other than null


1.  What is the actual issue here, what is the sequence of Exceptions and handlers (debug)

	When timeToWait < 0, the TimerExcpetion gets thrown as expected.  After the exception is handled in the catch block, timeNow is still set to null, which causes the problem.


1.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel)

	Added to repository (link: https://bitbucket.org/InfinityTerminated/exceptionrunner)


1.  Make a printScreen of your eclipse Maven test run, with console

	Added to repository (link: https://bitbucket.org/InfinityTerminated/exceptionrunner)


1.  What category of Exceptions is TimerException and what is NullPointerException

	TimerException is checked exception and NullPointerException is an unchecked exception (runtime).


1.  Push the updated/fixed source code to your own repository.

	Done.
